import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order (String food, int cost){
        int confirmation = JOptionPane.showConfirmDialog(null,
                food + "を注文しますか？",
                "注文の確認",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,food+"の注文を受け付けました。");
            String currentText;
            currentText = comment.getText();
            comment.setText( currentText + "\n"+ food + " : " +cost+"円");
            totalcost = cost + totalcost;
            textPane1.setText("Total cost is " + totalcost + "円");
        }
        if(confirmation == 1){

        }
    }
    private JPanel root;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton karageButton;
    private JButton ramenButton;
    private JButton yakisobaButton;
    private JButton tempuraButton;
    private JTextPane comment;
    private JButton 注文確認Button;
    private JTextPane textPane1;

    int totalcost;

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order(" 天ぷら",100);
            }
        });
        karageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("唐揚げ",100);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("餃子",100);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("うどん",100);
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("焼きそば",100);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ラーメン",100);
            }
        });
        注文確認Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "注文を終了しますか？",
                        "注文の確認",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,"注文が完了しました。ありがとうございます。");
                    comment.setText("");
                    textPane1.setText("Total cost is " + 0 + "円");
                    totalcost = 0;
                }
                if(confirmation == 1){
                }
            }
        });
    }
}
